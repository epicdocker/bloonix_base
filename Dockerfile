ARG DEBIAN_VERSION=9.4-slim

#### https://github.com/tests-always-included/mo/releases
FROM debian:${DEBIAN_VERSION} AS mo_buildenv
ENV MO_VERSION="2.0.4"
ENV MO_FILENAME="${MO_VERSION}.tar.gz"
ENV MO_DOWNLOAD_URL="https://github.com/tests-always-included/mo/archive/${MO_FILENAME}" \
    MO_DOWNLOAD_CHECKSUM="8fdf55b0489a9eb82163a53a8391a0114a774a21a28f3e0d06d6aa4fdcf6d3b11847c5ad90f265ca3d270929ff190df03595ab6c422a0b69f8080f7988755b7c"
RUN apt-get -y update \
 && apt-get -y install wget \
                       apt-transport-https \
                       ca-certificates \
 && wget --timeout=30 --tries=10 --retry-connrefused -O /${MO_FILENAME} ${MO_DOWNLOAD_URL} \
 && sha512sum /${MO_FILENAME} \
 && echo ${MO_DOWNLOAD_CHECKSUM}  /${MO_FILENAME} | sha512sum -c --strict \
 && tar -xf /${MO_FILENAME} -C / \
 && mv /mo-${MO_VERSION}/mo / \
 && chmod +x /mo

#### https://download.bloonix.de/repos/debian/
FROM debian:${DEBIAN_VERSION} AS bloonix_buildenv
ENV BLOONIX_GPG_FILENAME="bloonix.gpg"
ENV BLOONIX_GPG_DOWNLOAD_URL="https://download.bloonix.de/repos/debian/${BLOONIX_GPG_FILENAME}" \
    BLOONIX_GPG_CHECKSUM="0bd450c4aa6267457790ad2aff2666e153f36abe495c6671db0d72a0470de5bf1373fb0796118b3ae15e45cf0748688c3ae1a71c810df72713e5fa734c94ce21"
RUN apt-get -y update \
 && apt-get -y install wget \
                       apt-transport-https \
                       ca-certificates \
 && wget --timeout=30 --tries=10 --retry-connrefused -O /${BLOONIX_GPG_FILENAME} ${BLOONIX_GPG_DOWNLOAD_URL} \
 && sha512sum /${BLOONIX_GPG_FILENAME} \
 && echo ${BLOONIX_GPG_CHECKSUM}  /${BLOONIX_GPG_FILENAME} | sha512sum -c --strict

FROM debian:${DEBIAN_VERSION}
LABEL image.name="epicsoft_bloonix_base" \
      image.description="Base Docker image the opensource monitoring software Bloonix." \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV DEBIAN_VERSION_NAME="stretch"

RUN apt-get -y update \
 && apt-get -y install gnupg2 \
                       apt-transport-https \
                       openssl \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /opt/mo \
 && mkdir -p /opt/bloonix \
 && touch /bin/systemctl \
 && chmod +x /bin/systemctl

COPY --from=mo_buildenv [ "/mo", "/opt/mo/mo" ]
COPY --from=bloonix_buildenv [ "/bloonix.gpg", "/opt/bloonix/bloonix.gpg" ]
#### https://github.com/vishnubob/wait-for-it
COPY [ "wait-for-it.sh", "/usr/local/bin/wait-for-it" ]

RUN chmod 444 -R /opt/bloonix \
 && ln -s /opt/mo/mo /usr/local/bin/mo \
 && chmod +x /usr/local/bin/wait-for-it \
 && apt-key add /opt/bloonix/bloonix.gpg \
 && echo "deb https://download.bloonix.de/repos/debian/ ${DEBIAN_VERSION_NAME} main" > /etc/apt/sources.list.d/bloonix.list

ONBUILD RUN apt-get -y update \
         && apt-get -y install ca-certificates \
         && apt-get -y autoclean \
         && apt-get -y autoremove \
         && rm -rf /var/lib/apt/lists/*
