<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Links](#Links)
* 3. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Base Docker image for Bloonix 

This image provides a foundation for opensource monitoring Bloonix application in Docker.
It uses the official [Debian](https://hub.docker.com/_/debian/) image.
This image is extended with [Mo - Mustache Templates in Bash](https://github.com/tests-always-included/mo), [repository key](https://download.bloonix.de/repos/debian/) for Bloonix and Bash tool [wait-for-it](https://github.com/vishnubob/wait-for-it).


##  1. <a name='Versions'></a>Versions

The version numbers are separate versions and have no connection with the used applications in this Docker image. In the description you can see the version numbers of the integrated applications.

`latest` [Dockerfile](https://gitlab.com/epicdocker/bloonix_base/blob/master/Dockerfile)

`1.1.4` [Dockerfile](https://gitlab.com/epicdocker/bloonix_base/blob/release-1.1.4/Dockerfile)

`1.1.3` [Dockerfile](https://gitlab.com/epicdocker/bloonix_base/blob/release-1.1.3/Dockerfile)

`1.1.2` [Dockerfile](https://gitlab.com/epicdocker/bloonix_base/blob/release-1.1.2/Dockerfile)


###  1.1. <a name='Details'></a>Details

`1.1.4` `latest`

- See version `1.1.2`
- Update curl to current version `7.52.1-5+deb9u6`.
  - https://security-tracker.debian.org/tracker/CVE-2018-1000301

`1.1.3` 

- See version `1.1.2`
- Update OpenSSL to current version `1.1.0f-3+deb9u2`.
  - https://security-tracker.debian.org/tracker/CVE-2017-3738
  - https://security-tracker.debian.org/tracker/CVE-2018-0739
  - https://security-tracker.debian.org/tracker/CVE-2018-0733

`1.1.2`

- Used the official [Debian](https://hub.docker.com/_/debian/) image in version `9.4-slim`.
- Used the script [Mo - Mustache Templates in Bash](https://github.com/tests-always-included/mo) in version `2.0.4`.
- Installs the `current` [repository key](https://download.bloonix.de/repos/debian/) from Bloonix.
- Installs Bash tool [wait-for-it](https://github.com/vishnubob/wait-for-it) with the status of `06.02.2018`.


##  2. <a name='Links'></a>Links 

- Bloonix server Docker image - https://gitlab.com/epicdocker/bloonix_server
- Bloonix agent Docker image - https://gitlab.com/epicdocker/bloonix_agent
- Bloonix satellite Docker image - https://gitlab.com/epicdocker/bloonix_satellite
- Source Code - https://gitlab.com/epicdocker/bloonix_base
- GitLab Registry - https://gitlab.com/epicdocker/bloonix_base/container_registry
- Docker Hub Registry - https://hub.docker.com/r/epicsoft/bloonix_base/
- Bloonix OpenSource Monitoring Software - https://bloonix-monitoring.org/


##  3. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/bloonix_base/blob/master/LICENSE)
